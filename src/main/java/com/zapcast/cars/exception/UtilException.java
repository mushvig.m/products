package com.zapcast.cars.exception;

import com.zapcast.cars.util.ErrorData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UtilException extends BaseException {

  public UtilException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle, errorData);
  }

  public UtilException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle, parameter, errorData);
  }

  public static UtilException somethingWentWrong() {
    return new UtilException(ErrorTitle.SOMETHING_WENT_WRONG, null);
  }

  public static UtilException feignFailed(String message) {
    return new UtilException(ErrorTitle.FEIGN_FAILED, message, null);
  }

  public static UtilException validationFailed(String message) {
    return new UtilException(ErrorTitle.VALIDATION_FAILED, message, null);
  }

}
