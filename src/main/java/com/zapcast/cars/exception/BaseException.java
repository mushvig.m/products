package com.zapcast.cars.exception;

import com.zapcast.cars.util.ErrorData;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class BaseException extends RuntimeException {

  private final ErrorTitle errorTitle;
  private final ErrorData errorData;

  public BaseException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle.getMessage());
    this.errorTitle = errorTitle;
    this.errorData = errorData;
  }

  public BaseException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle.getMessage().replace("{}", parameter.toString()));
    this.errorTitle = errorTitle;
    this.errorData = errorData;
  }
}
