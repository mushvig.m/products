package com.zapcast.cars.rest;

import com.zapcast.cars.model.dto.CarDeleteRequestDto;
import com.zapcast.cars.model.dto.CarRequestDto;
import com.zapcast.cars.model.dto.CarUpdateRequestDto;
import com.zapcast.cars.service.CarService;
import com.zapcast.cars.util.ServiceResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class CarController {
  private final CarService carService;
  @PostMapping
  public ResponseEntity<ServiceResponse<?>> add(@RequestHeader("User-Id") Long userId,
                                                   @Valid @RequestBody CarRequestDto requestDto) {
    carService.createCar(userId, requestDto);

    return ResponseEntity.ok(ServiceResponse.success());
  }
  @PostMapping(value = "/update")
  public ResponseEntity<ServiceResponse<?>> update(@RequestHeader("User-Id") Long userId,
                                                   @Valid @RequestBody CarUpdateRequestDto requestDto) {
    carService.updateCar(userId, requestDto);

    return ResponseEntity.ok(ServiceResponse.success());
  }
  @PostMapping(value = "/delete")
  public ResponseEntity<ServiceResponse<?>> delete(@RequestHeader("User-Id") Long userId,
                                                   @Valid @RequestBody CarDeleteRequestDto requestDto) {
    carService.deleteCar(userId, requestDto);

    return ResponseEntity.ok(ServiceResponse.success());
  }
  @GetMapping
  public ResponseEntity<ServiceResponse<?>> list(@RequestHeader("User-Id") Long userId) {


    return ResponseEntity.ok(ServiceResponse.success(carService.getCarsByUserId(userId)));
  }
}
