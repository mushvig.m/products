package com.zapcast.cars.rest;

import com.zapcast.cars.service.ModelService;
import com.zapcast.cars.model.dto.ModelRequestDto;
import com.zapcast.cars.util.ServiceResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class ModelController {
  private final ModelService service;

  @GetMapping(value = "/models")
  public ResponseEntity<ServiceResponse<?>> models(@RequestBody ModelRequestDto dto) {

    return ResponseEntity.ok(
        ServiceResponse.success(service.models(dto)));
  }
}
