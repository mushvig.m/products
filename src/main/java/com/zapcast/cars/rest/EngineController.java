package com.zapcast.cars.rest;

import com.zapcast.cars.model.enums.EngineType;
import com.zapcast.cars.util.ServiceResponse;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class EngineController {

  @GetMapping(value = "/engine")
  public ResponseEntity<ServiceResponse<?>> year() {
    List<Integer> engine = new ArrayList<>();
    for (EngineType year : EngineType.values()) {
      engine.add(year.getValue());
    }
    return ResponseEntity.ok(ServiceResponse.success(engine));
  }
}

