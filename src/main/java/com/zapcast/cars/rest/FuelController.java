package com.zapcast.cars.rest;

import com.zapcast.cars.model.enums.FuelType;
import com.zapcast.cars.util.ServiceResponse;
import java.util.HashMap;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class FuelController {

  @GetMapping(value = "/fuel")
  public ResponseEntity<ServiceResponse<?>> fuel() {
    var result = Stream.of(FuelType.values())
        .map(industry -> new HashMap<String, String>() {{
          put("key", industry.name());
          put("value", industry.getDescription());
        }})
        .toList();


    return ResponseEntity.ok(ServiceResponse.success(result));
  }
}
