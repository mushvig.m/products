package com.zapcast.cars.rest;

import com.zapcast.cars.model.dto.MarksRequest;
import com.zapcast.cars.model.dto.MarkResponseDto;
import com.zapcast.cars.service.MarkService;
import com.zapcast.cars.util.ServiceResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class MarkController {
  private final MarkService service;

  @GetMapping(value = "/marks")
  public ResponseEntity<ServiceResponse<?>> marks() {

    return ResponseEntity.ok(
        ServiceResponse.success(service.marks()));
  }

  @PostMapping(value = "/marks/details")

  public List<MarkResponseDto> marksDetails(@RequestBody MarksRequest marks) {
    return service.marksDetails(marks);
  }
}
