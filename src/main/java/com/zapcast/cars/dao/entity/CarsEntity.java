package com.zapcast.cars.dao.entity;

import com.zapcast.cars.model.enums.CarCountry;
import com.zapcast.cars.model.enums.EngineType;
import com.zapcast.cars.model.enums.FuelType;
import com.zapcast.cars.model.enums.Transmission;
import com.zapcast.cars.model.enums.Years;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "cars")
public class CarsEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "user_id")
  private Long userId;
  @Column(name = "make_id")
  private Long makeId;
  @Column(name = "model_id")
  private Long modelId;
  @Column(name = "fuel_id")
  private FuelType fuelId;
  @Column(name = "year")
  private Years year;
  @Column(name = "engine_id")
  private EngineType engineId;
  @Column(name = "country_id")
  private CarCountry countryId;
  @Column(name = "transmission_id")
  private Transmission transmissionId;
  @Column(name = "vin_code")
  private String vinCode;
  private String isDeleted;


  @CreationTimestamp
  private Instant createdAt;

  @UpdateTimestamp
  private Instant lastUpdatedAt;
}
