package com.zapcast.cars.dao.repository;

import com.zapcast.cars.dao.entity.CarsEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<CarsEntity, Long> {
  Optional<CarsEntity> findByIdAndUserId(Long id, Long userId);
  List<CarsEntity> findByUserIdAndIsDeleted(Long userId, String isDeleted);
}
