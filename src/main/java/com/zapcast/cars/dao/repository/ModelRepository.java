package com.zapcast.cars.dao.repository;

import com.zapcast.cars.dao.entity.ModelEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelRepository extends JpaRepository<ModelEntity, Long> {
  List<ModelEntity> findAllByIdCarMake(Long idCarMake);
}
