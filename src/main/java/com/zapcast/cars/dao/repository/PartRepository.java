package com.zapcast.cars.dao.repository;

import com.zapcast.cars.dao.entity.PartsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartRepository extends JpaRepository<PartsEntity, Long> {
}
