package com.zapcast.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdcutsApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/products");
		SpringApplication.run(ProdcutsApplication.class, args);
	}

}
