package com.zapcast.cars.util;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class SendSmsHelper {
//  private final SmsClient smsClient;

  private final OtpGeneration otpGeneration;


  public void sendSms(String msisdn, String otp) {
//    try {
//
//      smsClient.send(OtpRequestDto);
//      }
//    } catch (FeignException.FeignClientException e) {
//      log.error("Error sending OTP", e);
//      throw new OtpException("send-otp-error", e.getMessage());
//    }
  }



  public String create(String msisdn) {
    return otpGeneration.create(msisdn);
  }


}
