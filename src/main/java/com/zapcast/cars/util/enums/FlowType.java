package com.zapcast.cars.util.enums;

public enum FlowType {
  LOGIN, REGISTER, CHANGE_PASSWORD
}
