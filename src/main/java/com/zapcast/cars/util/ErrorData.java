package com.zapcast.cars.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorData implements Serializable {

  @JsonProperty("expiration-time")
  private Long expirationTime;

  @JsonProperty("access-token")
  private String accessToken;

  @JsonProperty("device-token")
  private String deviceToken;

  @JsonProperty("role")
  private String role;


}
