package com.zapcast.cars.util;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zapcast.cars.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@JsonInclude(NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class ServiceResponse<T> {

    private ResponseStatus status;
    private ErrorResponse errorResponse;
    private T data;

    public static ServiceResponse<Void> success() {
        return ServiceResponse.<Void>builder()
                .status(ResponseStatus.OK)
                .build();
    }

    public static <T> ServiceResponse<T> success(T data) {
        return ServiceResponse.<T>builder()
//        .status(ResponseStatus.OK)
                .data(data)
                .build();
    }

    public static ServiceResponse<Void> error(BaseException baseException) {
        return ServiceResponse.<Void>builder()
//        .status(ResponseStatus.FAIL)
                .errorResponse(
                        new ErrorResponse(
                                baseException.getErrorTitle(),
                                null,
                                baseException.getMessage(),
                                baseException.getErrorData()))
                .build();
    }

    public static <T> ServiceResponse<T> error(BaseException baseException, T data) {
        return ServiceResponse.<T>builder()
//        .status(ResponseStatus.FAIL)
                .errorResponse(
                        new ErrorResponse(
                                baseException.getErrorTitle(),
                                null,
                                baseException.getMessage(),
                                baseException.getErrorData()))
                .data(data)
                .build();
    }
}
