package com.zapcast.cars.util;

public enum OtpStatus {

  SUCCESS,
  FAIL,
  EXPIRED,
  BLOCKED
}
