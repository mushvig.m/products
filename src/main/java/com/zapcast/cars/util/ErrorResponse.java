package com.zapcast.cars.util;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zapcast.cars.exception.ErrorTitle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@JsonInclude(NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {

  private ErrorTitle errorCode;
  private ErrorTitle title;

  private String message;
  private ErrorData data;

}
