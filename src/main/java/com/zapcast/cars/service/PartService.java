package com.zapcast.cars.service;

import com.zapcast.cars.dao.entity.PartsEntity;
import com.zapcast.cars.dao.repository.PartRepository;
import com.zapcast.cars.model.dto.PartRequestDto;
import com.zapcast.cars.model.enums.PartStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartService {
  private final PartRepository partRepository;

  public void createPart(Long userId, PartRequestDto request){
    var part = PartsEntity.builder()
        .userId(userId)
        .carId(request.getCarId())
        .title(request.getTitle())
        .description(request.getDescription())
        .activity(PartStatus.ACTIVE)
        .build();
    partRepository.save(part);
  }

  public Page<PartsEntity> getAllParts(int page, int size) {
    return partRepository.findAll(PageRequest.of(page, size));
  }
}
