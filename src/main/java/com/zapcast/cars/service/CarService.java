package com.zapcast.cars.service;

import com.zapcast.cars.dao.entity.CarsEntity;
import com.zapcast.cars.dao.repository.CarRepository;
import com.zapcast.cars.dao.repository.MarkRepository;
import com.zapcast.cars.dao.repository.ModelRepository;
import com.zapcast.cars.model.dto.CarDeleteRequestDto;
import com.zapcast.cars.model.dto.CarListDto;
import com.zapcast.cars.model.dto.CarRequestDto;
import com.zapcast.cars.model.dto.CarUpdateRequestDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarService {
  private final CarRepository carRepository;
  private MarkRepository markRepository;
  private ModelRepository modelRepository;


  public void createCar(Long userId, CarRequestDto request){
    var car = CarsEntity.builder()
        .userId(userId)
        .makeId(request.getMakeId())
        .modelId(request.getModelId())
        .engineId(request.getEngineId())
        .fuelId(request.getFuelId())
        .countryId(request.getCountryId())
        .transmissionId(request.getTransmissionId())
        .vinCode(request.getVinCode())
        .year(request.getYear())
        .isDeleted("N")
        .build();
    carRepository.save(car);
  }

  public void updateCar(Long userId, CarUpdateRequestDto request){
    var car = carRepository.findByIdAndUserId(request.getCarId(), userId).orElseThrow();// todo return error
        car.setMakeId(request.getMakeId());
        car.setModelId(request.getModelId());
        car.setEngineId(request.getEngineId());
        car.setFuelId(request.getFuelId());
        car.setCountryId(request.getCountryId());
        car.setTransmissionId(request.getTransmissionId());
        car.setVinCode(request.getVinCode());
        car.setYear(request.getYear());
    carRepository.save(car);
  }
  public void deleteCar(Long userId, CarDeleteRequestDto request){

    var car = carRepository.findByIdAndUserId(request.getCarId(),userId).orElseThrow();// todo return error
    car.setIsDeleted("Y");
    carRepository.save(car);
  }

  public List<CarListDto> getCarsByUserId(Long userId) {

    var cars = carRepository.findByUserIdAndIsDeleted(userId, "N");
    return cars.stream().map(this::convertToDto).collect(Collectors.toList());

  }
  private CarListDto convertToDto(CarsEntity car) {
    var mark = markRepository.findById(car.getMakeId()).orElse(null); //todo exception
    var model = modelRepository.findById(car.getModelId()).orElse(null); //todo exception

    return CarListDto.builder()
        .carId(car.getId())
        .carMake(mark.getName())
        .carModel(model.getName())
        .img(mark.getLogo())
        .vinCode(car.getVinCode())
        .build();

  }
}
