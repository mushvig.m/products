package com.zapcast.cars.service;


import com.zapcast.cars.dao.repository.MarkRepository;
import com.zapcast.cars.model.dto.MarkResponseDto;
import com.zapcast.cars.model.dto.MarksRequest;
import com.zapcast.cars.mapper.MarkMapper;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class MarkService {
  private final MarkRepository markRepository;

  public List<MarkResponseDto> marks(){
    var marks = markRepository.findAll();
    return MarkMapper.INSTANCE.entityToDtoList(marks);
  }
  public List<MarkResponseDto> marksDetails(@RequestBody MarksRequest marks){
    var  markIds = marks.getMarksList();
    var list = markRepository.findAllById(markIds);
      return MarkMapper.INSTANCE.entityToDtoList(list);
  }
}
