package com.zapcast.cars.service;

import com.zapcast.cars.model.dto.ModelResponseDto;
import com.zapcast.cars.dao.repository.ModelRepository;
import com.zapcast.cars.mapper.ModelMapper;
import com.zapcast.cars.model.dto.ModelRequestDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ModelService {
  private final ModelRepository repository;

  public List<ModelResponseDto> models(ModelRequestDto dto) {
    var modelEntities = repository.findAllByIdCarMake(dto.getMakeId());
    return ModelMapper.INSTANCE.entityToDtoList(modelEntities);
  }
}
