package com.zapcast.cars.mapper;

import com.zapcast.cars.dao.entity.MarkEntity;
import com.zapcast.cars.model.dto.MarkResponseDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class MarkMapper {
  public static final MarkMapper INSTANCE = Mappers.getMapper(MarkMapper.class);

  public abstract List<MarkResponseDto> entityToDtoList(List<MarkEntity> markEntities);

}
