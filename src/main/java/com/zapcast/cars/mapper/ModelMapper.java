package com.zapcast.cars.mapper;

import com.zapcast.cars.model.dto.ModelResponseDto;
import com.zapcast.cars.dao.entity.ModelEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class ModelMapper {
  public static final ModelMapper INSTANCE = Mappers.getMapper(ModelMapper.class);

  public abstract List<ModelResponseDto> entityToDtoList(List<ModelEntity> modelEntities);

}
