package com.zapcast.cars.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaginationRequestDto {
  private int page;
  private int size;
}
