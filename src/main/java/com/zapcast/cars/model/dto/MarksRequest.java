package com.zapcast.cars.model.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MarksRequest {
  private List<Long> marksList;
}
