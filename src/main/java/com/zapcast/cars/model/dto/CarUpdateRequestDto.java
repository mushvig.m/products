package com.zapcast.cars.model.dto;

import com.zapcast.cars.model.enums.CarCountry;
import com.zapcast.cars.model.enums.EngineType;
import com.zapcast.cars.model.enums.FuelType;
import com.zapcast.cars.model.enums.Transmission;
import com.zapcast.cars.model.enums.Years;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarUpdateRequestDto {
  private Long carId;
  private Long makeId;
  private Long modelId;
  private FuelType fuelId;
  private Years year;
  private EngineType engineId;
  private CarCountry countryId;
  private Transmission transmissionId;
  private String vinCode;
}
