package com.zapcast.cars.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartRequestDto {
  private Long carId;
  private String code;
  private String title;
  private String description;
}
