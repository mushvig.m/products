package com.zapcast.cars.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ModelResponseDto {
  private Long id;
  private String name;
}
