package com.zapcast.cars.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class MarkResponseDto {
  private String logo;
  private String name;
}
