package com.zapcast.cars.model.dto;

import com.zapcast.cars.model.enums.Years;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CarListDto {
  private Long carId;
  private String img;
  private String carMake;
  private String carModel;
  private Years year;
  private String vinCode;
}
