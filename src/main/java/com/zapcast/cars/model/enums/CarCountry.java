package com.zapcast.cars.model.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CarCountry {
  EUROPE( "Avropa"),
  JAPAN( "Yapon"),
  USA( "Amerika");

  private final String description;
}
