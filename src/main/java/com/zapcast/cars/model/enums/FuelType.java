package com.zapcast.cars.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FuelType {


  PETROL( "Benzin"),
  DİZEL( "Dizel"),
  GAS( "Qaz"),
  ELECTRO( "Elektro"),
  HYBRİD("Hibrid"),
  PLUG_İN_HYBRİD("Plug-in Hibrid");
  private final String description;
}
