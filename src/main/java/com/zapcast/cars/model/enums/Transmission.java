package com.zapcast.cars.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Transmission {
  MECHANICAL( "Mexaniki"),
  AUTOMATIC( "Avtomat"),
  ROBOT( "Robotlaşdırılmış"),
  VARIATOR( "Variator");

  private final String description;
}
